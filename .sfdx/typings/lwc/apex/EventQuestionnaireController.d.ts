declare module "@salesforce/apex/EventQuestionnaireController.getQuestions" {
  export default function getQuestions(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/EventQuestionnaireController.submit" {
  export default function submit(param: {recordId: any, jsonString: any}): Promise<any>;
}
