@isTest
public class CustomersControllerTest {
    
    public static String CRON_EXP = '0 0 * * * ?'; 
    
    @testSetup static void setup() 
    {
        Skie_Variable_Unleashed__c setting = new Skie_Variable_Unleashed__c();
        setting.Page_Size__c = 200;
        setting.Customers_Last_Sync__c = System.today();
        setting.Unleashed_API_Id__c = 'api-id';
        setting.Unleashed_API_Key__c = 'api-key';
        setting.Unleashed_Domain_Url__c = 'callout';
        
        insert setting;
        
        Account account = new Account();
        account.Name = 'test add';
        account.Guid__c = '43d7af78-73bd-44fa-9925-abc5624cd78a';
        insert account;
        
        Contact contact = new Contact();
        contact.Email = 'test@gmail.com';
        contact.AccountId = account.Id;
        contact.LastName = 'test';
        insert contact;
    }
    
    @isTest static void testSyncCustomer_SuccessCase() {
        HttpResponse customerResponse = new HttpResponse();
        customerResponse.setStatusCode(200);
        customerResponse.setBody(UnleashedHttpCalloutMockTest.getCustomersResponseForGET());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/Customers/1' => customerResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(200, 'OK', null, expectedResponse);
        
        Test.startTest();
        
        System.schedule('Sync Customers records created in Unleashed to SF', CRON_EXP, new CustomersSchedule());
        
        Test.stopTest();
    }
    
    @isTest static void testSyncCustomer_ErrorCase() {
        HttpResponse customerResponse = new HttpResponse();
        customerResponse.setStatusCode(500);
        customerResponse.setBody(UnleashedHttpCalloutMockTest.getErrorResponse());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/Customers/1' => customerResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(500, 'Internal Server Error', null, expectedResponse);
        
        Test.startTest();
        
        System.schedule('Sync Customers records created in Unleashed to SF', CRON_EXP, new CustomersSchedule());
        
        Test.stopTest();
    }
    
    @isTest static void testSyncCustomerByGuid() {
        HttpResponse customerResponse = new HttpResponse();
        customerResponse.setStatusCode(200);
        customerResponse.setBody(UnleashedHttpCalloutMockTest.getCustomerResponseForGET());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/Customers/xxx' => customerResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(200, 'OK', null, expectedResponse);
        
        Test.startTest();
        
        CustomersController.upsertRecordByGuid('Customers', 'xxx');
        
        Test.stopTest();
    }
}