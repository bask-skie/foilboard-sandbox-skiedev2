public class EventQuestionnaireController
{    
    public static final String CONTENT_DOCUMENT_LINK_SHARE_TYPE = 'V';
    
    @AuraEnabled
    public static Data getQuestions(Id recordId)
    {
        Event event = [SELECT Id, AccountId FROM Event WHERE Id = :recordId];
        
        List<Question__c> questions = queryQuestions();

        Data data = new Data();
        data.eventId = event.Id;
        data.questions = new List<Question>();
        
        for(Question__c qst : questions)
        {                        
            Question question = new Question();
            question.id = qst.Id;            
            question.label = qst.Question_Text__c;            
            question.answers = new List<Answer>();
            question.images = new List<Image>();
            
            if (qst.Answers__r != NULL)
            {                
                for(Answer__c ans : qst.Answers__r)
                {
                    Answer answer = new Answer();
                    answer.id = ans.id;
                    answer.label = ans.Name;
                    answer.value = ans.Id+','+qst.Id;
                    answer.requiredComment = ans.Require_Comments__c;
                    answer.requiredImage = ans.Require_Image__c;
                    question.answers.add(answer);
                }                
            }
            data.questions.add(question);
        }        
        
        return data;
    }
    
    public static List<Question__c> queryQuestions()
    {
        List<Question__c> questions = new List<Question__c>();
        Date currentDate = System.today();
        
        
        List<Question__c> questionList = [SELECT Id, Name, Question_Text__c,
                                          (select Name, Require_Comments__c, Require_Image__c from Answers__r)
                                          FROM Question__c
                                          WHERE Start_Date__c <= :currentDate
                                          AND End_Date__c >= :currentDate
                                          order by CreatedDate];
        return questionList;
    }
    
    @AuraEnabled
    public static void submit(String recordId, String jsonString)
    {
        Event event = [SELECT Id, AccountId FROM Event WHERE Id = :recordId];
        System.debug('jsonString: '+jsonString);
        
        Data data = (Data)JSON.deserialize(jsonString, Data.class);
        
        List<Question_Responses__c> responses = new List<Question_Responses__c>();
        List<ContentVersion> contentVersions = new List<ContentVersion>();
        List<ContentDocumentLink> contentDocumentLinkList = new List<ContentDocumentLink>();     
        Map<String, Question> questionMap = new Map<String, Question>();
        
        Savepoint sp = Database.setSavepoint();
        {                        
            for (Question question : data.questions)
            {                
                questionMap.put(question.id, question);                          
                Question_Responses__c response = new Question_Responses__c();                
                response.Answer__c = question.answerId;
                response.Question__c = question.id;
                response.Account__c = event.AccountId;      
                if (question.requiredComment)
                {
                    response.Response_Comments__c = question.comment;
                }
                responses.add(response);
            }
            
            insert responses;
             
            for (Question_Responses__c response : responses)
            {
                Question question = questionMap.get(response.Question__c);
                for (Integer i=0; i<question.images.size(); i++)
                {
                    Image img = question.images.get(i);
                    if(img.imageBase64Data != null)
                    {
                        ContentVersion cv = new ContentVersion();
                        cv.VersionData = EncodingUtil.base64Decode(img.imageBase64Data);
                        cv.Title = img.imageFileName;
                        cv.PathOnClient = img.imageFileName;
                        cv.Description = img.imageFileName;
                        cv.Question_Responses__c = response.Id;
                        contentVersions.add(cv);                    
                    }
                }                
            }
            
            if(contentVersions.size() > 0)
            {
                insert contentVersions;
                
                List<ContentVersion> cvList = [SELECT ContentDocumentId, Question_Responses__c FROM ContentVersion WHERE Id in: contentVersions];
                for(ContentVersion cv : cvList)
                {
                    ContentDocumentLink cdl = new ContentDocumentLink();
                    cdl.ContentDocumentId = cv.ContentDocumentId;
                    cdl.LinkedEntityId = cv.Question_Responses__c;
                    cdl.ShareType = CONTENT_DOCUMENT_LINK_SHARE_TYPE;                         
                    contentDocumentLinkList.add(cdl);
                }
                
                insert contentDocumentLinkList;
            }
		 
        }
        return;
    }    
    
    public class Data
    {
        @AuraEnabled public List<Question> questions;
        @AuraEnabled public String eventId;
    }
    
    public class Question
    {
        @AuraEnabled public String id;
        @AuraEnabled public String label;
        @AuraEnabled public Boolean mandatory;
        @AuraEnabled public List<Answer> answers;
        @AuraEnabled public String selectedAnswerId;
        @AuraEnabled public String answerId;
        @AuraEnabled public String responseId;
        @AuraEnabled public String campaignId;
        @AuraEnabled public Boolean requiredComment;
        @AuraEnabled public String comment;
        @AuraEnabled public Boolean requiredImage;
        @AuraEnabled public List<Image> images;
    }
    
    public class Answer
    {
        @AuraEnabled public String id;
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public Boolean requiredComment;
        @AuraEnabled public Boolean requiredImage;
    }
    
    public class Image
    {
        @AuraEnabled public String imageType;
        @AuraEnabled public String imageBase64Data;
        @AuraEnabled public String imageFileName;          
    }
}