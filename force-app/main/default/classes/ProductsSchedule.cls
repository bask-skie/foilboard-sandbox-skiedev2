global class ProductsSchedule Implements Schedulable {
    
    public static final String PRODUCTS = 'Products';
    
    global void execute(SchedulableContext ctx)
    {
        Skie_Variable_Unleashed__c setting = Skie_Variable_Unleashed__c.getOrgDefaults();
        DateTime lastSync = setting.Products_Last_Sync__c;
        
        setting.Products_Last_Sync__c = System.now();
        update setting;
        
        ProductsController productController = new ProductsController(lastSync, PRODUCTS, 1);
        productController.enqueueCallout();
    }
}