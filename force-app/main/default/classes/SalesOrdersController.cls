public class SalesOrdersController implements Queueable, Database.AllowsCallouts 
{
    private static final String UNLEASHED_SALES_ORDER_OBJECT = 'SalesOrders';
    private static final String SALESFORCE_SALES_ORDER_OBJECT = 'Sales Order';
    private static final String SALESFORCE_SALES_ORDER_LINE_OBJECT = 'Sales Order Line';
    
    private DateTime lastSync;
    private String objectName;
    private Integer page;
    
    public SalesOrdersController(DateTime lastSync, String objectName, Integer page)
    {
        this.lastSync = lastSync;
        this.objectName = objectName;
        this.page = page;
    }
    
    public Id enqueueCallout() 
    {
        Id jobId = System.enqueueJob(this);
        
        Integer queueableUsed = Limits.getQueueableJobs();
        Integer queueableLimit = Limits.getLimitQueueableJobs();
        
        return jobId;
    }
    
    public void execute(QueueableContext context)
    {
        HttpResponse response = UnleashedCallout.callOut(UnleashedUtils.HTTP_GET, lastSync, objectName, PAGE);
        
        if(response.getStatusCode() == 200)
        {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            String jsonString = JSON.serialize((Object) results.get(UnleashedUtils.PAGINATION_RESPONSE));
            Pagination pagination = (Pagination) JSON.deserialize(jsonString, Pagination.class);
            
            //callout salesorder for next page
            doProcessNextPage(pagination, this);
            
            String jsonItems = JSON.serialize((List<Object>) results.get(UnleashedUtils.RESPONSE_OBJECT));
            // replace field name that has reserved in SF
            jsonItems = UnleashedUtils.replaceReservedVariableName(jsonItems);
            
            List<SalesOrder> salesOrders = (List<SalesOrder>) JSON.deserialize(jsonItems, List<SalesOrder>.class);
            //process record for response
            doProcess(salesOrders);
        }
    }
    
    public static void doProcessNextPage(Pagination pagination, SalesOrdersController controller) 
    {
        if(controller.page < pagination.NumberOfPages)
        {
            Integer nextPage = controller.page;
            nextPage++;
            SalesOrdersController handleSync = new SalesOrdersController(controller.lastSync, controller.objectName, nextPage);
            handleSync.enqueueCallout();
        }
    }
    
    public static void upsertRecordByGuid(String objectName, String guid)
    {
        HttpResponse response = UnleashedCallout.callOut(UnleashedUtils.HTTP_GET, objectName, guid);
        if(response.getStatusCode() == 200)
        {
            Object result = (Object) JSON.deserializeUntyped(response.getBody());
            String jsonResult = JSON.serialize(result);
            
            // replace field name that has reserved in SF
            jsonResult = UnleashedUtils.replaceReservedVariableName(jsonResult);
            
            SalesOrder salesOrder = (SalesOrder) JSON.deserialize(jsonResult, SalesOrder.class);
            List<SalesOrder> salesOrders = new List<SalesOrder>();
            salesOrders.add(salesOrder);
            
            doProcess(salesOrders);
        }
    }
    
    private static void doProcess(List<SalesOrder> salesOrders)
    {
        Set<String> salesOrderGuidSet = new Set<String>();
        Set<String> salesOrderLineGuidSet = new Set<String>();
        Set<String> customerGuidSet = new Set<String>();
        Set<String> productGuidSet = new Set<String>();
        
        for(SalesOrder salesOrder : salesOrders)
        {
            salesOrderGuidSet.add(salesOrder.Guid);
            
            if(salesOrder.Customer != null)
            {
                customerGuidSet.add(salesOrder.Customer.Guid);
            }
            
            if(salesOrder.SalesOrderLines != null && salesOrder.SalesOrderLines.size() > 0)
            {
                for(SalesOrderLine orderLine : salesOrder.SalesOrderLines)
                {
                    salesOrderLineGuidSet.add(orderLine.Guid);
                    
                    if(orderLine.Product != null) { productGuidSet.add(orderLine.Product.Guid); }
                }
            }
        }
        
        List<Sales_Order__c> existingSalesOrders = [SELECT Id, Guid__c FROM Sales_Order__c WHERE Guid__c IN: salesOrderGuidSet];
        List<Account> existingCustomers = [SELECT Id, Guid__c FROM Account WHERE Guid__c IN: customerGuidSet];
        List<Sales_Order_Line__c> existingSalesOrderLines = [SELECT Id, Guid__c FROM Sales_Order_Line__c WHERE Guid__c IN: salesOrderLineGuidSet];
        List<Product2> existingProducts = [SELECT Id, Guid__c FROM Product2 WHERE Guid__c IN: productGuidSet];
        
        Map<String, Sales_Order__c> existingSalesOrdersMap = new Map<String, Sales_Order__c>();
        Map<String, Sales_Order_Line__c> existingSalesOrderLinesMap = new Map<String, Sales_Order_Line__c>();
        Map<String, Product2> existingProductMap = new Map<String, Product2>();
        Map<String, Account> existingCustomerMap = new Map<String, Account>();
        
        for(Sales_Order__c salesOrder : existingSalesOrders) { existingSalesOrdersMap.put(salesOrder.Guid__c, salesOrder); }
        
        for(Sales_Order_Line__c salesOrderLine : existingSalesOrderLines) { existingSalesOrderLinesMap.put(salesOrderLine.Guid__c, salesOrderLine); }
        
        for(Product2 product : existingProducts) { existingProductMap.put(product.Guid__c, product); }
        
        for(Account customer : existingCustomers) { existingCustomerMap.put(customer.Guid__c, customer); }
        
        // create tmpAccount if there are no existing account in SF
        createNewTmpAccounts(salesOrders, existingCustomerMap);
        
        // create tmpProduct if there are no existing product in SF
        createNewTmpProducts(salesOrders, existingProductMap);
        
        List<Sales_Order__c> listSalesOrderForUpsert = new List<Sales_Order__c>();
        List<Sales_Order_Line__c> listSalesOrderLineForUpsert = new List<Sales_Order_Line__c>();
        for(SalesOrder salesOrder : salesOrders)
        {
            
            Sales_Order__c salesOrderSF = buildSalesOrderSF(salesOrder, existingSalesOrdersMap, existingCustomerMap);
            listSalesOrderForUpsert.add(salesOrderSF);
        }
        
        if(listSalesOrderForUpsert.size() > 0)
        {
            List<Database.UpsertResult> results = Database.upsert(listSalesOrderForUpsert, false);
            List<String> guidFailedList = new List<String>();
            List<Object> errorList = new List<Object>();
            Integer index = 0;
            for(Database.UpsertResult result : results ) 
            {
                if(!result.isSuccess()) 
                {
                    Sales_Order__c obj = listSalesOrderForUpsert.get(index);
                    guidFailedList.add(obj.Guid__c);
                    errorList.add(result.getErrors());
                }
                index++;
            }
            if(guidFailedList.size() > 0 || errorList.size() > 0)
            {
                String guidFailedJson = JSON.serializePretty(guidFailedList);
                String errorJson = JSON.serializePretty(errorList);
                
                UnleashedUtils.addLog('Error', UNLEASHED_SALES_ORDER_OBJECT, SALESFORCE_SALES_ORDER_OBJECT, guidFailedJson, errorJson, null);
            }
        }
        
        Map<String, Sales_Order__c> salesOrdersMap = new Map<String, Sales_Order__c>();
        for(Sales_Order__c salesOrder : listSalesOrderForUpsert)
        {
            if(salesOrder.Id != null) { salesOrdersMap.put(salesOrder.Guid__c, salesOrder); }  
        }
        
        for(SalesOrder salesOrder : salesOrders)
        {
            String salesOrderId = salesOrdersMap.get(salesOrder.Guid).Id;
            if(salesOrderId != null)
            {
                if(salesOrder.SalesOrderLines != null && salesOrder.SalesOrderLines.size() > 0)
                {
                    for(SalesOrderLine orderLine : salesOrder.SalesOrderLines)
                    {
                        Sales_Order_Line__c salesOrderLineSF = buildSalesOrderLineSF(orderLine, salesOrderId, existingSalesOrderLinesMap, existingProductMap);
                        listSalesOrderLineForUpsert.add(salesOrderLineSF);
                    }
                }
            }
        }   
        
        if(listSalesOrderLineForUpsert.size() > 0)
        {
            List<Database.UpsertResult> results = Database.upsert(listSalesOrderLineForUpsert, false);
            List<String> guidFailedList = new List<String>();
            List<Object> errorList = new List<Object>();
            Integer index = 0;
            for(Database.UpsertResult result : results) 
            {
                if(!result.isSuccess()) 
                {
                    Sales_Order_Line__c obj = listSalesOrderLineForUpsert.get(index);
                    guidFailedList.add(obj.Guid__c);
                    errorList.add(result.getErrors());
                }
                index++;
            }
            if(guidFailedList.size() > 0 || errorList.size() > 0)
            {
                String guidFailedJson = JSON.serializePretty(guidFailedList);
                String errorJson = JSON.serializePretty(errorList);
                
                UnleashedUtils.addLog('Error', UNLEASHED_SALES_ORDER_OBJECT, SALESFORCE_SALES_ORDER_LINE_OBJECT, guidFailedJson, errorJson, null);
            }
        }
    }
    
    private static Sales_Order__c buildSalesOrderSF(SalesOrder salesOrder, Map<String, Sales_Order__c> existingSalesOrdersMap, Map<String, Account> existingCustomerMap){
        Sales_Order__c salesOrderSF = new Sales_Order__c();
        if(existingSalesOrdersMap.get(salesOrder.Guid) != null)
        {
            salesOrderSF.Id = existingSalesOrdersMap.get(salesOrder.Guid).Id;
        }
        
        if(salesOrder.Customer != null && existingCustomerMap.get(salesOrder.Customer.Guid) != null) 
        {
            salesOrderSF.Account__c = existingCustomerMap.get(salesOrder.Customer.Guid).Id;
        }
        
        salesOrderSF.BCSubTotal__c = salesOrder.BCSubTotal;
        salesOrderSF.BCTaxTotal__c = salesOrder.BCTaxTotal;
        salesOrderSF.BCTotal__c = salesOrder.BCTotal;
        salesOrderSF.Comments__c = salesOrder.Comments;
        salesOrderSF.CreatedBy__c = salesOrder.CreatedBy;
        salesOrderSF.CreatedOn__c = UnleashedUtils.convertMillisecToDate(salesOrder.CreatedOn);
        salesOrderSF.Currency__c = salesOrder.UnleashedCurrency != null ? salesOrder.UnleashedCurrency.CurrencyCode : null;
        salesOrderSF.CustomerRef__c = salesOrder.CustomerRef;
        salesOrderSF.DeliveryCity__c = salesOrder.DeliveryCity;
        salesOrderSF.DeliveryCountry__c = salesOrder.DeliveryCountry;
        salesOrderSF.DeliveryInstruction__c = salesOrder.DeliveryInstruction;
        salesOrderSF.DeliveryMethod__c = salesOrder.DeliveryMethod;
        salesOrderSF.DeliveryName__c = salesOrder.DeliveryName;
        salesOrderSF.DeliveryPostCode__c = salesOrder.DeliveryPostCode;
        salesOrderSF.DeliveryRegion__c = salesOrder.DeliveryRegion;
        salesOrderSF.DeliveryStreetAddress__c = salesOrder.DeliveryStreetAddress;
        salesOrderSF.DeliveryStreetAddress2__c = salesOrder.DeliveryStreetAddress2;
        salesOrderSF.DeliverySuburb__c = salesOrder.DeliverySuburb;
        salesOrderSF.DiscountRate__c = salesOrder.DiscountRate;
        salesOrderSF.ExchangeRate__c = salesOrder.ExchangeRate;
        salesOrderSF.Guid__c = salesOrder.Guid;
        salesOrderSF.LastModifiedBy__c = salesOrder.LastModifiedBy;
        salesOrderSF.LastModifiedOn__c = UnleashedUtils.convertMillisecToDate(salesOrder.LastModifiedOn);
        salesOrderSF.OrderDate__c = UnleashedUtils.convertMillisecToDate(salesOrder.OrderDate);
        salesOrderSF.Name = salesOrder.OrderNumber;
        salesOrderSF.OrderStatus__c = salesOrder.OrderStatus;
        salesOrderSF.PaymentDueDate__c = UnleashedUtils.convertMillisecToDate(salesOrder.PaymentDueDate);
        salesOrderSF.ReceivedDate__c = UnleashedUtils.convertMillisecToDate(salesOrder.ReceivedDate);
        salesOrderSF.RequiredDate__c = UnleashedUtils.convertMillisecToDate(salesOrder.RequiredDate);
        salesOrderSF.CompletedDate__c = UnleashedUtils.convertMillisecToDate(salesOrder.CompletedDate);
        salesOrderSF.SalesOrderGroup__c = salesOrder.SalesOrderGroup;
        salesOrderSF.SalesPerson__c = salesOrder.SalesPerson != null ? salesOrder.SalesPerson.FullName : null;
        salesOrderSF.SourceId__c = salesOrder.SourceId;
        salesOrderSF.SubTotal__c = salesOrder.SubTotal;
        salesOrderSF.Tax__c = salesOrder.Tax != null ? salesOrder.Tax.TaxRate : 0;
        salesOrderSF.TaxRate__c = salesOrder.TaxRate;
        salesOrderSF.TaxTotal__c = salesOrder.TaxTotal;
        salesOrderSF.Total__c = salesOrder.Total;
        salesOrderSF.TotalVolume__c = salesOrder.TotalVolume;
        salesOrderSF.TotalWeight__c = salesOrder.TotalWeight;
        salesOrderSF.Warehouse__c = salesOrder.Warehouse != null ? salesOrder.Warehouse.WarehouseCode : null;
        salesOrderSF.XeroTaxCode__c = salesOrder.XeroTaxCode;
        return salesOrderSF;
    }
    
    private static Sales_Order_Line__c buildSalesOrderLineSF(SalesOrderLine orderLine, String salesOrderId, Map<String, Sales_Order_Line__c> existingSalesOrderLinesMap, Map<String, Product2> existingProductMap){
        Sales_Order_Line__c salesOrderLineSF = new Sales_Order_Line__c();
        if(existingSalesOrderLinesMap.get(orderLine.Guid) != null)
        {
            salesOrderLineSF.Id = existingSalesOrderLinesMap.get(orderLine.Guid).Id;
        }
        if(orderLine.Product != null && existingProductMap.get(orderLine.Product.Guid) != null) 
        {
            salesOrderLineSF.Product__c = existingProductMap.get(orderLine.Product.Guid).Id;
        }
        
        salesOrderLineSF.Name = String.valueOf(orderLine.LineNumber);
        salesOrderLineSF.BCLineTax__c = orderLine.BCLineTax;
        salesOrderLineSF.BCLineTotal__c = orderLine.BCLineTotal;
        salesOrderLineSF.BCUnitPrice__c = orderLine.BCUnitPrice;
        salesOrderLineSF.UnitCost__c = orderLine.UnitCost;
        salesOrderLineSF.UnitPrice__c = orderLine.UnitPrice;
        salesOrderLineSF.LineTax__c = orderLine.LineTax;
        salesOrderLineSF.LineTaxCode__c = orderLine.LineTaxCode;
        salesOrderLineSF.LineTotal__c = orderLine.LineTotal;
        salesOrderLineSF.LineType__c = orderLine.LineType;
        salesOrderLineSF.OrderQuantity__c = orderLine.OrderQuantity;
        salesOrderLineSF.Comments__c = orderLine.Comments;
        salesOrderLineSF.DiscountRate__c = orderLine.DiscountRate;
        salesOrderLineSF.Guid__c = orderLine.Guid;
        salesOrderLineSF.TaxRate__c = orderLine.TaxRate;
        salesOrderLineSF.DueDate__c = UnleashedUtils.convertMillisecToDate(orderLine.DueDate);
        salesOrderLineSF.LastModifiedOn__c = UnleashedUtils.convertMillisecToDate(orderLine.LastModifiedOn);
        salesOrderLineSF.Volume__c = orderLine.Volume;
        salesOrderLineSF.Weight__c = orderLine.Weight;
        salesOrderLineSF.XeroSalesAccount__c = orderLine.XeroSalesAccount;
        salesOrderLineSF.XeroTaxCode__c = orderLine.XeroTaxCode;
        salesOrderLineSF.Sales_Order__c = salesOrderId;
        return salesOrderLineSF;
    }
    
    private static void createNewTmpAccounts(List<SalesOrder> salesOrders, Map<String, Account> existingCustomerMap){
        List<Account> newTmpAccountList = new List<Account>();
        Set<String> newTmpAccountGuid = new Set<String>();
        
        Skie_Variable_Unleashed__c setting = Skie_Variable_Unleashed__c.getOrgDefaults();
        String accountRecordTypeId = setting.Account_RecordTypeId__c;
        List<RecordType> recordTypeList = [SELECT Id FROM RecordType WHERE Id =: accountRecordTypeId];
        String recordTypeId = recordTypeList.size() > 0 ? recordTypeList.get(0).Id : null;
        
        for(SalesOrder salesOrder : salesOrders)
        {
            if(existingCustomerMap.get(salesOrder.Customer.Guid) == null && !newTmpAccountGuid.contains(salesOrder.Customer.Guid))
            {
                Account newAccount = new Account(Name=salesOrder.Customer.CustomerCode, Account_Code__c=salesOrder.Customer.CustomerCode, Guid__c=salesOrder.Customer.Guid);
                if(recordTypeId != null) newAccount.RecordTypeId = recordTypeId;
                newTmpAccountList.add(newAccount);
                newTmpAccountGuid.add(salesOrder.Customer.Guid);
            }
        }
        
        if(newTmpAccountList.size() > 0)
        {
            insert newTmpAccountList;
            
            for(Account account : newTmpAccountList){
                existingCustomerMap.put(account.Guid__c, account);
            }
        }
    }
    
    private static void createNewTmpProducts(List<SalesOrder> salesOrders, Map<String, Product2> existingProductMap){
        List<Product2> newTmpProductList = new List<Product2>();
        Set<String> newTmpProductGuid = new Set<String>();
        for(SalesOrder salesOrder : salesOrders)
        {
            if(salesOrder.SalesOrderLines != null && salesOrder.SalesOrderLines.size() > 0)
            {
                for(SalesOrderLine orderLine : salesOrder.SalesOrderLines)
                {
                    if(existingProductMap.get(orderLine.Product.Guid) == null && !newTmpProductGuid.contains(orderLine.Product.Guid))
                    {
                        Product2 newProduct = new Product2(Name=orderLine.Product.ProductCode, ProductCode=orderLine.Product.ProductCode, Guid__c=orderLine.Product.Guid);
                        newTmpProductList.add(newProduct);
                        newTmpProductGuid.add(orderLine.Product.Guid);
                    }
                }
            }
        } 
        if(newTmpProductList.size() > 0)
        {
            insert newTmpProductList;
            
            for(Product2 product : newTmpProductList){
                existingProductMap.put(product.Guid__c, product);
            }
        }
    }
    
    public class SalesOrderLineProduct 
    {
        private String Guid {get; set;}
        private String ProductCode {get; set;}
    }
    
    public class SalesOrderLine 
    {
        private Double UnitCost {get; set;}
        private Double BCLineTax {get; set;}
        private Double BCLineTotal {get; set;}
        private Double BCUnitPrice {get; set;}
        private String Comments {get; set;}
        private Double DiscountRate {get; set;}
        private String DueDate {get; set;}
        private String Guid {get; set;}
        private String LastModifiedOn {get; set;}
        private Integer LineNumber {get; set;}
        private String LineType {get; set;}
        private Double LineTax {get; set;}
        private String LineTaxCode {get; set;}
        private Double LineTotal {get; set;}
        private Double OrderQuantity {get; set;}
        private SalesOrderLineProduct Product {get; set;}
        private Double TaxRate {get; set;}
        private Double UnitPrice {get; set;}
        private Double Volume {get; set;}
        private Double Weight {get; set;}
        private String XeroSalesAccount {get; set;}
        private String XeroTaxCode {get; set;}
    }
    
    public class SalesOrder 
    {
        private List<SalesOrderLine> SalesOrderLines {get; set;}
        private Double BCSubTotal {get; set;}
        private Double BCTaxTotal {get; set;}
        private Double BCTotal {get; set;}
        private String Comments {get; set;}
        private String CreatedBy {get; set;}
        private String CreatedOn {get; set;}
        private UnleashedCurrency UnleashedCurrency {get; set;}
        private Customer Customer {get; set;}
        private String CustomerRef {get; set;}
        private String DeliveryCity {get; set;}
        private String DeliveryCountry {get; set;}
        private String DeliveryInstruction {get; set;}
        private String DeliveryMethod {get; set;}
        private String DeliveryName {get; set;}
        private String DeliveryPostCode {get; set;}
        private String DeliveryRegion {get; set;}
        private String DeliveryStreetAddress {get; set;}
        private String DeliveryStreetAddress2 {get; set;}
        private String DeliverySuburb {get; set;}
        private Double DiscountRate {get; set;}
        private Double ExchangeRate {get; set;}
        private String Guid {get; set;}
        private String LastModifiedBy {get; set;}
        private String LastModifiedOn {get; set;}
        private String OrderDate {get; set;}
        private String OrderNumber {get; set;}
        private String OrderStatus {get; set;}
        private String PaymentDueDate {get; set;}
        private String ReceivedDate {get; set;}
        private String RequiredDate {get; set;}
        private String CompletedDate {get; set;}
        private String SalesOrderGroup {get; set;}
        private SalesPerson SalesPerson {get; set;}
        private String SourceId {get; set;}
        private Double SubTotal {get; set;}
        private Tax Tax {get; set;}
        private Double TaxRate {get; set;}
        private Double TaxTotal {get; set;}
        private Double Total {get; set;}
        private Double TotalVolume {get; set;}
        private Double TotalWeight {get; set;}
        private Warehouse Warehouse {get; set;}
        private String XeroTaxCode {get; set;}
    }
    
    public class Warehouse 
    {
        private String WarehouseCode {get; set;}
    }
    
    public class Tax 
    {
        private Double TaxRate {get; set;}
        private String Guid {get; set;}
    }    
    
    public class Customer 
    {
        private String Guid {get; set;}
        private String CustomerCode {get; set;}
    }
}