public class ProductsController implements Queueable, Database.AllowsCallouts {
    
    private static final String UNLEASHED_PRODUCT_OBJECT = 'Products';
    private static final String SALESFORCE_PRODUCT_OBJECT = 'Product';
    private static final String SALESFORCE_PRICEBOOK_ENTRY_OBJECT = 'PricebookEntry';
    
    private DateTime lastSync;
    private String objectName;
    private Integer page;
    
    public ProductsController(DateTime lastSync, String objectName, Integer page){
        this.lastSync = lastSync;
        this.objectName = objectName;
        this.page = page;
    }
    
    public Id enqueueCallout() {
        Id jobId = System.enqueueJob(this);
        
        Integer queueableUsed = Limits.getQueueableJobs();
        Integer queueableLimit = Limits.getLimitQueueableJobs();
        
        return jobId;
    }
    
    public void execute(QueueableContext context){
        HttpResponse response = UnleashedCallout.callOut(UnleashedUtils.HTTP_GET, lastSync, objectName, PAGE);
        if(response.getStatusCode() == 200)
        {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            String jsonString = JSON.serialize((Object) results.get(UnleashedUtils.PAGINATION_RESPONSE));
            Pagination pagination = (Pagination) JSON.deserialize(jsonString, Pagination.class);
            
            //callout product for next page
            doProcessNextPage(pagination, this);
            
            String jsonItems = JSON.serialize((List<Object>) results.get(UnleashedUtils.RESPONSE_OBJECT));
            
            List<Product> products = (List<Product>) JSON.deserialize(jsonItems, List<Product>.class);
            
            //process record for response
            doProcess(products);
        }
    }
    
    public static void doProcessNextPage(Pagination pagination, ProductsController controller) 
    {
        if(controller.page < pagination.NumberOfPages)
        {
            Integer nextPage = controller.page;
            nextPage++;
            ProductsController handleSync = new ProductsController(controller.lastSync, controller.objectName, nextPage);
            handleSync.enqueueCallout();
        }
    }
    
    public static void upsertRecordByGuid(String objectName, String guid)
    {
        HttpResponse response = UnleashedCallout.callOut(UnleashedUtils.HTTP_GET, objectName, guid);
        if(response.getStatusCode() == 200)
        {
            Object result = (Object) JSON.deserializeUntyped(response.getBody());
            String jsonResult = JSON.serialize(result);
            Product product = (Product) JSON.deserialize(jsonResult, Product.class);
            List<Product> products = new List<Product>();
            products.add(product);
            
            doProcess(products);
        }
    }
    
    private static void doProcess(List<Product> products)
    {
        Set<String> productGuidSet = new Set<String>();
        for(Product product : products)
        {
            productGuidSet.add(product.Guid);
        }
        
        List<Product2> existingProducts = [SELECT Id, Guid__c FROM Product2 WHERE Guid__c IN: productGuidSet];
        
        Map<String, Product2> existingProductMap = new Map<String, Product2>();
        for(Product2 product : existingProducts)
        {
            existingProductMap.put(product.Guid__c, product);
        }
        
        List<Product2> listProductForUpsert = new List<Product2>();
        for(Product product : products)
        {
            Product2 productSF = buildProductSF(product, existingProductMap);
            listProductForUpsert.add(productSF);
        }
        if(listProductForUpsert.size() > 0)
        {
            List<Database.UpsertResult> results = Database.upsert(listProductForUpsert, false);
            List<String> guidFailedList = new List<String>();
            List<Object> errorList = new List<Object>();
            Integer index = 0;
            for(Database.UpsertResult result : results ) 
            {
                if(!result.isSuccess()) 
                {
                    Product2 obj = listProductForUpsert.get(index);
                    guidFailedList.add(obj.Guid__c);
                    errorList.add(result.getErrors());
                }
                index++;
            }
            if(guidFailedList.size() > 0 || errorList.size() > 0)
            {
                String guidFailedJson = JSON.serializePretty(guidFailedList);
                String errorJson = JSON.serializePretty(errorList);
                
                UnleashedUtils.addLog('Error', UNLEASHED_PRODUCT_OBJECT, SALESFORCE_PRODUCT_OBJECT, guidFailedJson, errorJson, null);
            }
        }
        
        Set<String> productIdSet = new Set<String>();
        Map<String, Product2> productMap = new Map<String, Product2>(listProductForUpsert);
        Map<String, List<Double>> pricebookProductValue = new Map<String, List<Double>>();
        for(Product2 product : listProductForUpsert)
        {
            if(product.Id != null)
            {
                productIdSet.add(product.Id);
                pricebookProductValue.put(product.Id, getSellPriceTierValueList(product));
            }
        }
        Skie_Variable_Unleashed__c setting = Skie_Variable_Unleashed__c.getOrgDefaults(); 
        List<PricebookEntry> existingPricebookEntry = [SELECT Id, Product2Id, Pricebook2Id FROM PricebookEntry WHERE Product2Id IN: productIdSet];
        Map<String, PricebookEntry> priceBookEntryMap = new Map<String, PricebookEntry>();
        
        for(PricebookEntry priceBookEntry : existingPricebookEntry)
        {
            priceBookEntryMap.put(priceBookEntry.Pricebook2Id + '' + priceBookEntry.Product2Id, priceBookEntry);
        }

        List<String> pricebookIdList = getPricebookIdList();
        List<PricebookEntry> listStandardPricebookEntry = new List<PricebookEntry>();
        List<PricebookEntry> listPricebookEntryForUpsert = new List<PricebookEntry>();
        for(Product2 product : listProductForUpsert)
        {
            if(product.id != null)
            {
                if(priceBookEntryMap.get(setting.StandardPriceBookID__c + '' + product.Id) == null)
                {
                    PricebookEntry standardPricebook = new PricebookEntry(Pricebook2Id=setting.StandardPriceBookID__c, Product2Id=product.Id
                                                                          , UnitPrice=0, IsActive=true);
                    listStandardPricebookEntry.add(standardPricebook);
                }
                Integer index = 0;
                for(String pricebookId : pricebookIdList)
                {
                    PricebookEntry priceBook = new PricebookEntry();
                    priceBook.Pricebook2Id = pricebookId;
                    priceBook.Product2Id = product.Id;
                    priceBook.UnitPrice = pricebookProductValue.get(product.Id).get(index);
                    priceBook.IsActive = true;
                    if(priceBookEntryMap.get(pricebookId + '' + product.Id) != null)
                    {
                        priceBook.id = priceBookEntryMap.get(pricebookId + '' + product.Id).Id;
                    }
                    listPricebookEntryForUpsert.add(priceBook);
                    index++;
                }
            }
            
        }
        if(listStandardPricebookEntry.size() > 0){
            List<Database.SaveResult> results = Database.insert(listStandardPricebookEntry, false);
            List<String> guidFailedList = new List<String>();
            List<Object> errorList = new List<Object>();
            Integer index = 0;
            for(Database.SaveResult result : results) 
            {
                if(!result.isSuccess())
                {
                    PricebookEntry obj = listStandardPricebookEntry.get(index);
                    guidFailedList.add(productMap.get(obj.Product2Id).Guid__c);
                    errorList.add(result.getErrors());
                }
                index++;
            }
            if(guidFailedList.size() > 0 || errorList.size() > 0)
            {
                String guidFailedJson = JSON.serializePretty(guidFailedList);
                String errorJson = JSON.serializePretty(errorList);
                
                UnleashedUtils.addLog('Error', UNLEASHED_PRODUCT_OBJECT, SALESFORCE_PRICEBOOK_ENTRY_OBJECT, guidFailedJson, errorJson, null);
            }
        }
        
        if(listPricebookEntryForUpsert.size() > 0)
        {            
            List<Database.UpsertResult> results = Database.upsert(listPricebookEntryForUpsert, false);
            List<String> guidFailedList = new List<String>();
            List<Object> errorList = new List<Object>();
            Integer index = 0;
            for(Database.UpsertResult result : results ) 
            {
                if(!result.isSuccess())
                {
                    PricebookEntry obj = listPricebookEntryForUpsert.get(index);
                    guidFailedList.add(productMap.get(obj.Product2Id).Guid__c);
                    errorList.add(result.getErrors());
                }
                index++;
            }
            if(guidFailedList.size() > 0 || errorList.size() > 0)
            {
                String guidFailedJson = JSON.serializePretty(guidFailedList);
                String errorJson = JSON.serializePretty(errorList);
                
                UnleashedUtils.addLog('Error', UNLEASHED_PRODUCT_OBJECT, SALESFORCE_PRICEBOOK_ENTRY_OBJECT, guidFailedJson, errorJson, null);
            }
        }
        
    }
    
    private static Product2 buildProductSF(Product product, Map<String, Product2> existingProductMap){
        Product2 productSF = new Product2();
        if(existingProductMap.get(product.Guid) != null)
        {
            productSF.Id = existingProductMap.get(product.Guid).Id;
        }
        productSF.AverageLandPrice__c = product.AverageLandPrice;
        productSF.Barcode__c = product.Barcode;
        productSF.BinLocation__c = product.BinLocation;
        productSF.CreatedBy__c  = product.CreatedBy;
        productSF.CreatedOn__c = UnleashedUtils.convertMillisecToDate(product.CreatedOn);
        productSF.CustomerSellPrice__c = product.CustomerSellPrice;
        productSF.DefaultSellPrice__c = product.DefaultSellPrice;
        productSF.DefaultPurchasePrice__c = product.DefaultPurchasePrice;
        productSF.Depth__c = product.Depth;
        productSF.Guid__c = product.Guid;
        productSF.Height__c = product.Height;
        productSF.IsAssembledProduct__c = product.IsAssembledProduct;
        productSF.ImageUrl__c = product.ImageUrl;
        productSF.IsComponent__c = product.IsComponent;
        productSF.IsActive = product.IsSellable;
        productSF.IsSellable__c  = product.IsSellable;
        productSF.LastCost__c = product.LastCost;
        productSF.LastModifiedBy__c = product.LastModifiedBy;
        productSF.LastModifiedOn__c = UnleashedUtils.convertMillisecToDate(product.LastModifiedOn);
        productSF.MaxStockAlertLevel__c = product.MaxStockAlertLevel;
        productSF.MinStockAlertLevel__c = product.MinStockAlertLevel;
        productSF.MinimumOrderQuantity__c = product.MinimumOrderQuantity;
        productSF.MinimumSaleQuantity__c = product.MinimumSaleQuantity;
        productSF.MinimumSellPrice__c = product.MinimumSellPrice;
        productSF.NeverDiminishing__c = product.NeverDiminishing;
        productSF.Notes__c = product.Notes;
        productSF.Obsolete__c = product.Obsolete;
        productSF.PackSize__c = product.PackSize;
        productSF.ProductCode = product.ProductCode;
        productSF.Name = product.ProductDescription.length() > 250 ? product.ProductDescription.left(250) : product.ProductDescription;
        productSF.Description = product.ProductDescription;
        productSF.Family = product.ProductGroup != null ? product.ProductGroup.GroupName : null;
        productSF.ReOrderPoint__c = product.ReOrderPoint;
        productSF.SellPriceTier1__c = product.SellPriceTier1 != null ? product.SellPriceTier1.Value : null;
        productSF.SellPriceTier2__c = product.SellPriceTier2 != null ? product.SellPriceTier2.Value : null;
        productSF.SellPriceTier3__c = product.SellPriceTier3 != null ? product.SellPriceTier3.Value : null;
        productSF.SellPriceTier4__c = product.SellPriceTier4 != null ? product.SellPriceTier4.Value : null;
        productSF.SellPriceTier5__c = product.SellPriceTier5 != null ? product.SellPriceTier5.Value : null;
        productSF.SellPriceTier6__c = product.SellPriceTier6 != null ? product.SellPriceTier6.Value : null;
        productSF.SellPriceTier7__c = product.SellPriceTier7 != null ? product.SellPriceTier7.Value : null;
        productSF.SellPriceTier8__c = product.SellPriceTier8 != null ? product.SellPriceTier8.Value : null;
        productSF.SellPriceTier9__c = product.SellPriceTier9 != null ? product.SellPriceTier9.Value : null;
        productSF.SellPriceTier10__c = product.SellPriceTier10 != null ? product.SellPriceTier10.Value : null;
        productSF.SourceId__c = product.SourceId;
        productSF.SourceVariantParentId__c = product.SourceVariantParentId;
        productSF.SupplierProductCode__c = product.Supplier != null ? product.Supplier.SupplierCode : null;
        productSF.Supplier__c = product.Supplier != null ? product.Supplier.SupplierName : null;
        productSF.TaxablePurchase__c = product.TaxablePurchase;
        productSF.TaxableSales__c = product.TaxableSales;
        productSF.UnitOfMeasure__c = product.UnitOfMeasure != null ? product.UnitOfMeasure.Name : null;
        productSF.Weight__c = product.Weight;
        productSF.Width__c = product.Width;
        productSF.XeroCostOfGoodsAccount__c = product.XeroCostOfGoodsAccount;
        productSF.XeroSalesAccount__c = product.XeroSalesAccount;
        productSF.XeroSalesTaxCode__c = product.XeroSalesTaxCode;
        productSF.XeroSalesTaxRate__c = product.XeroSalesTaxRate;
        productSF.XeroTaxCode__c = product.XeroTaxCode;
        productSF.XeroTaxRate__c = product.XeroTaxRate;
        return productSF;
    }
    
    private static List<Double> getSellPriceTierValueList(Product2 product) {
        List<Double> value = new List<Double>();
        value.add(getSellPriceTierValue(product.SellPriceTier1__c));
        value.add(getSellPriceTierValue(product.SellPriceTier2__c));
        value.add(getSellPriceTierValue(product.SellPriceTier3__c));
        value.add(getSellPriceTierValue(product.SellPriceTier4__c));
        value.add(getSellPriceTierValue(product.SellPriceTier5__c));
        value.add(getSellPriceTierValue(product.SellPriceTier6__c));
        value.add(getSellPriceTierValue(product.SellPriceTier7__c));
        value.add(getSellPriceTierValue(product.SellPriceTier8__c));
        value.add(getSellPriceTierValue(product.SellPriceTier9__c));
        value.add(getSellPriceTierValue(product.SellPriceTier10__c));
        return value;
    }
    
    private static Double getSellPriceTierValue(Decimal value) {
        return value != null ? value : 0;
    }
    
    private static List<String> getPricebookIdList() {
        Skie_Variable_Unleashed__c setting = Skie_Variable_Unleashed__c.getOrgDefaults(); 
        List<String> pricebookIdList = new List<String>();
        pricebookIdList.add(setting.Tier_1_Pricebook_Id__c);
        pricebookIdList.add(setting.Tier_2_Pricebook_Id__c);
        pricebookIdList.add(setting.Tier_3_Pricebook_Id__c);
        pricebookIdList.add(setting.Tier_4_Pricebook_Id__c);
        pricebookIdList.add(setting.Tier_5_Pricebook_Id__c);
        pricebookIdList.add(setting.Tier_6_Pricebook_Id__c);
        pricebookIdList.add(setting.Tier_7_Pricebook_Id__c);
        pricebookIdList.add(setting.Tier_8_Pricebook_Id__c);
        pricebookIdList.add(setting.Tier_9_Pricebook_Id__c);
        pricebookIdList.add(setting.Tier_10_Pricebook_Id__c);
        return pricebookIdList;
    }
        
    public class SellPriceTier {
        private String Name {get; set;}
        private Double Value {get; set;}
    }
    
    public class ProductGroupDTO {
        private String Guid {get; set;}
        private String GroupName {get; set;}
    }
    
    public class SupplierDTO {
        private String Guid {get; set;}
        private String SupplierCode {get; set;}
        private String SupplierName {get; set;}
    }
    
    public class UnitOfMeasureDTO {
        private String Guid {get; set;}
        private String Name {get; set;}
    }
    
    public class Product {
        private Double AverageLandPrice {get; set;}
        private String Barcode {get; set;}
        private String BinLocation {get; set;}
        private String CreatedBy {get; set;}
        private String CreatedOn {get; set;}
        private Double CustomerSellPrice {get; set;}
        private Double DefaultPurchasePrice {get; set;}
        private Double DefaultSellPrice {get; set;}
        private Double Depth {get; set;}
        private String Guid {get; set;}
        private Double Height {get; set;}
        private Boolean IsAssembledProduct {get; set;}
        private String ImageUrl {get; set;}
        private Boolean IsComponent {get; set;}
        private Boolean IsSellable {get; set;}
        private Double LastCost {get; set;}
        private String LastModifiedBy {get; set;}
        private String LastModifiedOn {get; set;}
        private Double MaxStockAlertLevel {get; set;}
        private Double MinStockAlertLevel {get; set;}
        private Double MinimumOrderQuantity {get; set;}
        private Double MinimumSaleQuantity {get; set;}
        private Double MinimumSellPrice {get; set;}
        private Boolean NeverDiminishing {get; set;}
        private String Notes {get; set;}
        private Boolean Obsolete {get; set;}
        private Double PackSize {get; set;}
        private String ProductCode {get; set;}
        private String ProductDescription {get; set;}
        private ProductGroupDTO ProductGroup {get; set;}
        private Double ReOrderPoint {get; set;}
        private SellPriceTier SellPriceTier1 {get; set;}
        private SellPriceTier SellPriceTier2 {get; set;}
        private SellPriceTier SellPriceTier3 {get; set;}
        private SellPriceTier SellPriceTier4 {get; set;}
        private SellPriceTier SellPriceTier5 {get; set;}
        private SellPriceTier SellPriceTier6 {get; set;}
        private SellPriceTier SellPriceTier7 {get; set;}
        private SellPriceTier SellPriceTier8 {get; set;}
        private SellPriceTier SellPriceTier9 {get; set;}
        private SellPriceTier SellPriceTier10 {get; set;}
        private String SourceId {get; set;}
        private String SourceVariantParentId {get; set;}
        private SupplierDTO Supplier {get; set;}
        private Boolean TaxablePurchase {get; set;}
        private Boolean TaxableSales {get; set;}
        private UnitOfMeasureDTO  UnitOfMeasure {get; set;}
        private Double Weight {get; set;}
        private Double Width {get; set;}
        private String XeroCostOfGoodsAccount {get; set;}
        private String XeroSalesAccount {get; set;}
        private String XeroSalesTaxCode {get; set;}
        private Double XeroSalesTaxRate {get; set;}
        private String XeroTaxCode {get; set;}
        private Double XeroTaxRate {get; set;}
        
    }
}