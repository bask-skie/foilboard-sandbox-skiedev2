public class UnleashedCallout {
    
    public static Skie_Variable_Unleashed__c setting = Skie_Variable_Unleashed__c.getOrgDefaults();
    
    private static Integer RES_SUCCESS  = 200;
    private static final String ACCEPT = 'Accept';
    private static final String APPLICATION_JSON = 'application/json';
    private static final String CONTENT_TYPE = 'Content-Type';
    private static final String API_AUTH_ID = 'api-auth-id';
    private static final String API_AUTH_SIGNATURE = 'api-auth-signature';
    private static final String DOMAIN = 'https://api.unleashedsoftware.com';
    private static final String MODIFIED_SINCE = 'modifiedSince';
    private static final String PAGE_SIZE = 'pageSize';
        
    public static HttpResponse callOut(String method, DateTime lastSync, String objectName, Integer page){
        String domain = setting.Unleashed_Domain_Url__c != null ? setting.Unleashed_Domain_Url__c : DOMAIN;
        String path = domain + '/' + objectName + '/' + page;
        String queryParam = '';
        if(setting.Page_Size__c != null)
        {
            Integer pageSize = Integer.valueOf(setting.Page_Size__c) > 1000 ? 1000 : Integer.valueOf(setting.Page_Size__c); 
            queryParam += PAGE_SIZE + '=' + pageSize;
        }
        if(lastSync != null)
        {
            if(setting.Page_Size__c != null)
            {
                queryParam += '&';
            }
            queryParam += MODIFIED_SINCE + '=' + getModifiedDateFilterFormatted(lastSync);
        }
        HttpResponse response = executeCallout(path, queryParam, method, objectName);
        return response;
    }
    
    public static HttpResponse callOut(String method, String objectName, String guid){
        String domain = setting.Unleashed_Domain_Url__c != null ? setting.Unleashed_Domain_Url__c : DOMAIN;
        String path = domain + '/' + objectName + '/' + guid;
        HttpResponse response = executeCallout(path, null, method, objectName);
        return response;
    }
    
    public static HttpResponse executeCallout(String path, String queryParam, String method, String objectName){
        queryParam = queryParam != null ? queryParam : '';
        String endpoint = queryParam != '' ? path + '?' + queryParam : path;
		system.debug('### Endpoint : ' + endpoint);
        
        HttpRequest request = new HttpRequest();
        request.setMethod(method);
        request.setEndpoint(endpoint);
        request.setHeader(ACCEPT, APPLICATION_JSON);
        request.setHeader(CONTENT_TYPE, APPLICATION_JSON);
        request.setHeader(API_AUTH_ID, setting.Unleashed_API_Id__c);
        request.setHeader(API_AUTH_SIGNATURE, getSignature(queryParam));
        //execute callout
        HttpResponse response = new Http().send(request);
        
        system.debug('### Response Status: ' + response.getStatusCode() + ': ' + response.getStatus());
        system.debug('### Response Body: ' + response.getBody());
        system.debug('===========================');
        
        if(response.getStatusCode() != 200)
        {
            String headerJSON = getResponseHeaderJSON(response);
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            UnleashedUtils.addLog('Error', objectName, endpoint, JSON.serializePretty(results), headerJSON);
        }

        return response;
    }
    
    public static String getResponseHeaderJSON(HttpResponse response) 
    {
        Map<String, Object> responseHeaderMap = new Map<String, Object>();
        String returnValue = '';
        
        try 
        {
            String[] headerKeys = response.getHeaderKeys();
            for(String key : headerKeys) 
            {
                responseHeaderMap.put(key, response.getHeader(key));
            }
            
            if(responseHeaderMap.size() > 0) 
            {
                returnValue = JSON.serializePretty(responseHeaderMap, true);
            }
            
        } catch (Exception ex) {
            System.debug('### Error with getResponseHeaderJSON : '+ex.getStackTraceString());
            returnValue = 'Error during mapping response header to JSON: error='+ex.getMessage();
        }
        
        return returnValue;
    }
    
    public static String getSignature(String queryParam){
        String apiKey = setting.Unleashed_API_Key__c != null ? setting.Unleashed_API_Key__c : '-';
        Blob apiKeyBlob = Blob.valueOf(apiKey);
        Blob urlBlob = Blob.valueOf(queryParam);
        Blob signatureBlob = Crypto.generateMac('HmacSHA256', urlBlob, apiKeyBlob);
        
        String signature = EncodingUtil.base64Encode(signatureBlob);
        
        return signature;
    }
    
    public static String getModifiedDateFilterFormatted (DateTime sfDateTime) {
        Integer offset = UserInfo.getTimezone().getOffset(sfDateTime);
        Datetime local = sfDateTime.addSeconds(offset/1000);
        String changedFormat = ((DateTime)sfDateTime).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS');
        return changedFormat;
    }
}