public class UnleashedUtils {
    public static final String HTTP_GET  = 'GET';
    public static final String RESPONSE_OBJECT = 'Items';
    public static final String PAGINATION_RESPONSE = 'Pagination';
	private static final Integer START_TEXT_MILLISEC = 6;
    private static final Integer END_TEXT_MILLISEC = 19;
    
    private static final String RESERVED_VARIABLE_CURRENCY = '"Currency"';
    private static final String REPLACE_VARIABLE_CURRENCY = '"UnleashedCurrency"';
    
    public static Date convertMillisecToDate(String dateString)
    {
        if(dateString != null)
        {
            long millisecond = long.valueOf(dateString.substring(START_TEXT_MILLISEC, END_TEXT_MILLISEC));
            DateTime dt = DateTime.newInstance(millisecond);
            Date sfDate = date.newinstance(dt.year(), dt.month(), dt.day());
            return sfDate;
        }
        return null;
    }
    
    public static String replaceReservedVariableName(String jsonString){
        return jsonString.replace(RESERVED_VARIABLE_CURRENCY, REPLACE_VARIABLE_CURRENCY);
    }
    
    public static String validateLogBodyLength(String log){
        return log.length() > 131000 ?  log.left(131000) : log;
    }
    
    public static void addLog(String type, String unleashedObj, String req, String res, String header){
        addLog(type, unleashedObj, null, req, res, header);
    }
    
    public static void addLog(String type, String unleashedObj, String salesforceObject, String req, String res, String header)
    {
        String requestPayload = validateLogBodyLength(req);
        String responsePayload = validateLogBodyLength(res);
        Log__c log = new Log__c(Type__c=type, Unleashed_Object__c=unleashedObj, Salesforce_Object__c=salesforceObject, Request_Payload__c=requestPayload, Response_Payload__c=responsePayload, Response_Header__c=header);
        insert log;
    }
}