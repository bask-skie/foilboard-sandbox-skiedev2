@isTest
public class EventQuestionnaireTest
{
    
    @testSetup
    static void setupData()
    {        
        Account account = new Account(Name = 'Account Test');
        insert account;        
        
        Event event = new Event();
        event.WhatId = account.Id;
        event.DurationInMinutes = 60;
        event.ActivityDateTime = System.now();
        insert event;


        Question__c question = new Question__c();
        question.Question_Text__c = 'What your name?';
        question.Start_Date__c = System.today();
        question.End_Date__c = System.today();
        question.Account__c = account.Id;
        insert question;
        
        Answer__c answer1 = new Answer__c();
        answer1.Name = 'Neo';
        answer1.Question__c = question.Id;
        insert answer1;
        
        Answer__c answer2 = new Answer__c();
        answer2.Name = 'Trinity';
        answer2.Question__c = question.Id;
        insert answer2;
           
    }
    
    @isTest
    static void test_getQuestions()
    {
        Event event = [SELECT Id FROM Event ORDER BY CreatedDate DESC LIMIT 1];
        Test.startTest();
        EventQuestionnaireController.Data data = EventQuestionnaireController.getQuestions(event.Id);        
        Test.stopTest();
        System.assertEquals(1, data.questions.size());        
    }
    
    @isTest
    static void test_saveQuestions()
    {
        Event event = [SELECT Id FROM Event ORDER BY CreatedDate DESC LIMIT 1];
        Test.startTest();
        EventQuestionnaireController.Data data = EventQuestionnaireController.getQuestions(event.Id);
        System.debug(data);
        EventQuestionnaireController.Image image = new EventQuestionnaireController.Image();
        image.imageBase64Data = 'testimagedata';
        image.imageFileName = 'testfilename';
        data.questions.get(0).answerId = [SELECT Id FROM Answer__c LIMIT 1].Id;
        data.questions.get(0).requiredComment = true;
        data.questions.get(0).comment = 'test comment';
        data.questions.get(0).images.add(image);
        String jsonString = JSON.serialize(data);
        EventQuestionnaireController.submit(event.Id, jsonString);
        Test.stopTest();
        System.assertEquals(1, data.questions.size());        
    }  


}