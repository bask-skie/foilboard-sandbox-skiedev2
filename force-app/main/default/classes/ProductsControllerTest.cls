@isTest
public class ProductsControllerTest {

    public static String CRON_EXP = '0 0 * * * ?'; 
    
    @testSetup static void setup() 
    {
        Pricebook2 pricebook = new Pricebook2(Name='Sell Price Tier 1', IsActive=true);
        insert pricebook;
        
        Skie_Variable_Unleashed__c setting = new Skie_Variable_Unleashed__c();
        setting.Page_Size__c = 200;
        setting.Products_Last_Sync__c = System.today();
        setting.Unleashed_API_Id__c = 'api-id';
        setting.Unleashed_API_Key__c = 'api-key';
        setting.Unleashed_Domain_Url__c = 'callout';
        setting.Tier_1_Pricebook_Id__c = pricebook.Id;
        insert setting;
        
        Product2 product = new Product2();
        product.Name = 'Test Product';
        product.Guid__c = 'c97c6b46-f1cc-4741-b236-f882995e7d9f';
        insert product;
     
    }
    
    @isTest static void testSyncProducts_SuccessCase() {
        HttpResponse productResponse = new HttpResponse();
        productResponse.setStatusCode(200);
        productResponse.setBody(UnleashedHttpCalloutMockTest.getProductsResponseForGET());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/Products/1' => productResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(200, 'OK', null, expectedResponse);
        
        Test.startTest();
        
        System.schedule('Sync Product records created in Unleashed to SF', CRON_EXP, new ProductsSchedule());
        
        Test.stopTest();
    }
    
    @isTest static void testSyncProductsAndPricebookEntry_SuccessCase() {
        Skie_Variable_Unleashed__c setting = Skie_Variable_Unleashed__c.getOrgDefaults(); 
        System.debug(setting);
        Product2 product = [SELECT Id FROM Product2 LIMIT 1];
        
        PricebookEntry standardPricebook = new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(), Product2Id=product.Id, IsActive=true, UnitPrice=1.0);
		insert standardPricebook;
        
        PricebookEntry pricebook = new PricebookEntry(Pricebook2Id=setting.Tier_1_Pricebook_Id__c, Product2Id=product.Id, IsActive=true, UnitPrice=1.0);
		insert pricebook;
        
        
        HttpResponse productResponse = new HttpResponse();
        productResponse.setStatusCode(200);
        productResponse.setBody(UnleashedHttpCalloutMockTest.getProductsResponseForGET());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/Products/1' => productResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(200, 'OK', null, expectedResponse);
        
        Test.startTest();
        
        System.schedule('Sync Product records created in Unleashed to SF', CRON_EXP, new ProductsSchedule());
        
        Test.stopTest();
    }
    
    @isTest static void testSyncCustomerByGuid() {
        HttpResponse productResponse = new HttpResponse();
        productResponse.setStatusCode(200);
        productResponse.setBody(UnleashedHttpCalloutMockTest.getProductResponseForGET());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/Products/xxx' => productResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(200, 'OK', null, expectedResponse);
        
        Test.startTest();
        
        ProductsController.upsertRecordByGuid('Products', 'xxx');
        
        Test.stopTest();
    }
    
    @isTest static void testSyncProduct_ErrorCase() {
        HttpResponse productResponse = new HttpResponse();
        productResponse.setStatusCode(500);
        productResponse.setBody(UnleashedHttpCalloutMockTest.getErrorResponse());
    
        Map<String, HttpResponse> expectedResponse = new Map<String, HttpResponse> {
            'callout/Products/1' => productResponse
        };
        
        UnleashedHttpCalloutMockTest.setTestMockResponse(500, 'Internal Server Error', null, expectedResponse);
        
        Test.startTest();
        
        System.schedule('Sync Product records created in Unleashed to SF', CRON_EXP, new ProductsSchedule());
        
        Test.stopTest();
    }
}