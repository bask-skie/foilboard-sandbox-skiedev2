global class CustomersSchedule implements Schedulable{

    public static final String CUSTOMERS = 'Customers';
        
    global void execute(SchedulableContext ctx)
    {
        Skie_Variable_Unleashed__c setting = Skie_Variable_Unleashed__c.getOrgDefaults();
        DateTime lastSync = setting.Customers_Last_Sync__c;
        
        setting.Customers_Last_Sync__c = System.now();
        update setting;
        
        CustomersController customersController = new CustomersController(lastSync, CUSTOMERS, 1);
        customersController.enqueueCallout();
    }
}