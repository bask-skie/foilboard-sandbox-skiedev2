public class CustomersController implements Queueable, Database.AllowsCallouts {
    
    private static final String BILLING_ADDRESS = 'Physical';
    private static final String SHIPPING_ADDRESS = 'Shipping';
    private static final String UNLEASHED_CUSTOMER_OBJECT = 'Customers';
    private static final String SALESFORCE_CUSTOMER_OBJECT = 'Account';
    private static final String SALESFORCE_CONTACT_OBJECT = 'Contact';
    
    private DateTime lastSync;
    private String objectName;
    private Integer page;
    
    public CustomersController(DateTime lastSync, String objectName, Integer page){
        this.lastSync = lastSync;
        this.objectName = objectName;
        this.page = page;
    }
    
    public Id enqueueCallout() {
        Id jobId = System.enqueueJob(this);
        
        Integer queueableUsed = Limits.getQueueableJobs();
        Integer queueableLimit = Limits.getLimitQueueableJobs();
        
        return jobId;
    }
    
    public void execute(QueueableContext context){
        HttpResponse response = UnleashedCallout.callOut(UnleashedUtils.HTTP_GET, lastSync, objectName, PAGE);
        
        if(response.getStatusCode() == 200)
        {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            String jsonString = JSON.serialize((Object) results.get(UnleashedUtils.PAGINATION_RESPONSE));
            Pagination pagination = (Pagination) JSON.deserialize(jsonString, Pagination.class);
            
            //callout customer for next page
            doProcessNextPage(pagination, this);
            
            String jsonItems = JSON.serialize((List<Object>) results.get(UnleashedUtils.RESPONSE_OBJECT));
            // replace field name that has reserved in SF
            jsonItems = UnleashedUtils.replaceReservedVariableName(jsonItems);
            
            List<Customer> customers = (List<Customer>) JSON.deserialize(jsonItems, List<Customer>.class);
            doProcess(customers);
            
        }
    }

	public static void doProcessNextPage(Pagination pagination, CustomersController controller) 
    {
        if(controller.page < pagination.NumberOfPages)
        {
            Integer nextPage = controller.page;
            nextPage++;
            CustomersController handleSync = new CustomersController(controller.lastSync, controller.objectName, nextPage);
            handleSync.enqueueCallout();
        }
    }
    
    public static void upsertRecordByGuid(String objectName, String guid)
    {
        HttpResponse response = UnleashedCallout.callOut(UnleashedUtils.HTTP_GET, objectName, guid);
        if(response.getStatusCode() == 200)
        {
            Object result = (Object) JSON.deserializeUntyped(response.getBody());
            String jsonResult = JSON.serialize(result);
            
            // replace field name that has reserved in SF
            jsonResult = UnleashedUtils.replaceReservedVariableName(jsonResult);
            
            Customer customer = (Customer) JSON.deserialize(jsonResult, Customer.class);
            List<Customer> customers = new List<Customer>();
            customers.add(customer);
            
            doProcess(customers);
        }
    }

    private static void doProcess(List<Customer> customers)
    {
        Set<String> customerGuidSet = new Set<String>();
        Set<String> contactEmailSet = new Set<String>();
        Map<String, Customer> customerGuidMap = new Map<String, Customer>();
        for(Customer customer : customers)
        {
            customerGuidSet.add(customer.Guid);
            customerGuidMap.put(customer.Guid, customer);
            if(customer.Email != null)
            {
                contactEmailSet.add(customer.Email);
            }
        }
        
        List<Account> existingAccounts = [SELECT Id, Guid__c FROM Account WHERE Guid__c IN: customerGuidSet];
        Map<String, Account> existingAccountMap = new Map<String, Account>();
        
        for(Account customer : existingAccounts)
        {
            existingAccountMap.put(customer.Guid__c, customer);
        }
        
        Skie_Variable_Unleashed__c setting = Skie_Variable_Unleashed__c.getOrgDefaults();
        String accountRecordTypeId = setting.Account_RecordTypeId__c;
        List<RecordType> recordTypeList = [SELECT Id FROM RecordType WHERE Id =: accountRecordTypeId];
        List<Account> listAccountForUpsert = new List<Account>();
        for(Customer customer : customers)
        {
            String recordTypeId = recordTypeList.size() > 0 ? recordTypeList.get(0).Id : null;
            Account customerSF = buildAccountSF(customer, existingAccountMap, recordTypeId);
            listAccountForUpsert.add(customerSF);
        }
        
        if(listAccountForUpsert.size() > 0)
        {
            
            List<Database.UpsertResult> accountUpsertResults = Database.upsert(listAccountForUpsert, false);
            List<String> accountGuidFailedList = new List<String>();
            List<Object> accountErrorList = new List<Object>();
            Integer index = 0;
            for(Database.UpsertResult result : accountUpsertResults ) 
            {
                if(!result.isSuccess()) 
                {
                    Account obj = listAccountForUpsert.get(index);
                    accountGuidFailedList.add(obj.Guid__c);
                    accountErrorList.add(result.getErrors());
                }
                index++;
            }
            if(accountGuidFailedList.size() > 0 || accountErrorList.size() > 0)
            {
                String guidFailedJson = JSON.serializePretty(accountGuidFailedList);
                String errorJson = JSON.serializePretty(accountErrorList);
                
                UnleashedUtils.addLog('Error', UNLEASHED_CUSTOMER_OBJECT, SALESFORCE_CUSTOMER_OBJECT, guidFailedJson, errorJson, null);
            }
        }
        
        List<Contact> existingContacts = [SELECT Id, Email, AccountId FROM Contact Where Email IN: contactEmailSet AND AccountId <> null];
        Map<String, Contact> existingContactMap = new Map<String, Contact>();
        for(Contact contact : existingContacts)
        {
            existingContactMap.put(contact.AccountId + contact.Email, contact);
        }
        
        List<Contact> listContactForUpsert = new List<Contact>();
        for(Account account : listAccountForUpsert)
        {
            Customer tmpContact = customerGuidMap.get(account.Guid__c);
            String tempContactName = tmpContact.ContactLastName != null ? tmpContact.ContactLastName : tmpContact.ContactFirstName != null ? tmpContact.ContactFirstName : tmpContact.Email;
            if(account.Id != null && tmpContact != null && tempContactName != null)
            {
                Contact contactSF = buildContactSF(account, tmpContact, existingContactMap);
                listContactForUpsert.add(contactSF);
            }
        }
        if(listContactForUpsert.size() > 0)
        {            
            List<Database.UpsertResult> results = Database.upsert(listContactForUpsert, false);
            List<String> guidFailedList = new List<String>();
            List<Object> errorList = new List<Object>();
            Integer index = 0;
            for(Database.UpsertResult result : results ) 
            {
                if(!result.isSuccess())
                {
                    Account obj = listAccountForUpsert.get(index);
                    guidFailedList.add(obj.Guid__c);
                    errorList.add(result.getErrors());
                }
                index++;
            }
            if(guidFailedList.size() > 0 || errorList.size() > 0)
            {
                String guidFailedJson = JSON.serializePretty(guidFailedList);
                String errorJson = JSON.serializePretty(errorList);
                
                UnleashedUtils.addLog('Error', UNLEASHED_CUSTOMER_OBJECT, SALESFORCE_CONTACT_OBJECT, guidFailedJson, errorJson, null);
            }
        }
    }   
    
    private static Account buildAccountSF(Customer customer, Map<String, Account> existingAccountMap, String recordTypeId){
        Account customerSF = new Account();
        if(existingAccountMap.get(customer.Guid) != null)
        {
            customerSF.Id = existingAccountMap.get(customer.Guid).Id;
        }
        Address billingAddr = getAddressFromType(customer.Addresses, BILLING_ADDRESS);
        customerSF.BillingStreet = getAddressStreet(billingAddr);
        customerSF.BillingCity = billingAddr != null ? billingAddr.City : null;
        customerSF.BillingState = billingAddr != null ? billingAddr.Region : null;
        customerSF.BillingCountry = billingAddr != null ? billingAddr.Country : null;
        customerSF.BillingPostalCode = billingAddr != null ? billingAddr.PostalCode : null;
        
        Address shippingAddr = getAddressFromType(customer.Addresses, SHIPPING_ADDRESS);
        customerSF.ShippingStreet = getAddressStreet(shippingAddr);
        customerSF.ShippingCity = shippingAddr != null ? shippingAddr.City : null;
        customerSF.ShippingState = shippingAddr != null ? shippingAddr.Region : null;
        customerSF.ShippingCountry = shippingAddr != null ? shippingAddr.Country : null;
        customerSF.ShippingPostalCode = shippingAddr != null ? shippingAddr.PostalCode : null;

        if(recordTypeId != null) customerSF.RecordTypeId = recordTypeId;
        customerSF.BankAccount__c = customer.BankAccount;
        customerSF.BankBranch__c = customer.BankBranch;
        customerSF.BankName__c = customer.BankName;
        customerSF.CreatedBy__c = customer.CreatedBy;
        customerSF.CreatedOn__c = UnleashedUtils.convertMillisecToDate(customer.CreatedOn);
        customerSF.Currency__c = customer.UnleashedCurrency != null ? customer.UnleashedCurrency.CurrencyCode : null;
        customerSF.Account_Code__c = customer.CustomerCode;
        customerSF.Name = customer.CustomerName.length() > 120 ? customer.CustomerName.left(120) : customer.CustomerName;
        customerSF.Customer_Name__c = customer.CustomerName;
        customerSF.Type = customer.CustomerType;
        customerSF.DDINumber__c = customer.DDINumber;
        customerSF.DeliveryInstruction__c = customer.DeliveryInstruction;
        customerSF.DiscountRate__c = customer.DiscountRate;
        customerSF.EmailCC__c = customer.EmailCC;
        customerSF.Fax = customer.FaxNumber;
        customerSF.ABN__c = customer.GSTVATNumber;
        customerSF.Guid__c = customer.Guid;
        customerSF.LastModifiedBy__c = customer.LastModifiedBy;
        customerSF.LastModifiedOn__c = UnleashedUtils.convertMillisecToDate(customer.LastModifiedOn);
        customerSF.Notes__c = customer.Notes;
        customerSF.Obsolete__c = customer.Obsolete;
        customerSF.PaymentTerm__c = customer.PaymentTerm;
        customerSF.Phone = customer.PhoneNumber;
        customerSF.PrintInvoice__c = customer.PrintInvoice != null ? customer.PrintInvoice : false;
        customerSF.PrintPackingSlipInsteadOfInvoice__c = customer.PrintPackingSlipInsteadOfInvoice != null ? customer.PrintPackingSlipInsteadOfInvoice : false;
        customerSF.SalesPerson__c = customer.SalesPerson != null ? customer.SalesPerson.FullName : null;
        customerSF.SellPriceTier__c = customer.SellPriceTier;
        customerSF.SellPriceTierReference__c = customer.SellPriceTierReference != null ? customer.SellPriceTierReference.Reference : null;
        customerSF.StopCredit__c = customer.StopCredit;
        customerSF.Taxable__c = customer.Taxable != null ? customer.Taxable : false;
        customerSF.TaxCode__c = customer.TaxCode;
        customerSF.TaxRate__c = customer.TaxRate;
        customerSF.TollFreeNumber__c = customer.TollFreeNumber;
        customerSF.Website = customer.Website;
        customerSF.XeroCostOfGoodsAccount__c = customer.XeroCostOfGoodsAccount;
        customerSF.XeroSalesAccount__c = customer.XeroSalesAccount;
        return customerSF;
    }
    
    private static Contact buildContactSF(Account account, Customer tmpContact, Map<String, Contact> existingContactMap){
        Contact contactSF = new Contact();
        if(existingContactMap.get(account.Id + tmpContact.Email) != null)
        {
            contactSF.Id = existingContactMap.get(account.Id + tmpContact.Email).Id;
        }
        contactSF.AccountId = account.Id;
        contactSF.FirstName = tmpContact.ContactFirstName;
        contactSF.LastName = tmpContact.ContactLastName != null ? tmpContact.ContactLastName : tmpContact.ContactFirstName != null ? tmpContact.ContactFirstName : tmpContact.Email;
        contactSF.Email = tmpContact.Email;
        contactSF.MobilePhone = tmpContact.MobileNumber;
        return contactSF;
    }
    
    private static String getAddressStreet(Address address) {
        String street = '';
        if(address != null)
        {
            if(address.AddressName != null)
            {
                street += address.AddressName + ' ';
            }
            if(address.StreetAddress != null)
            {
                street += address.StreetAddress + ' ';
            }
            if(address.StreetAddress2 != null)
            {
                street += address.StreetAddress2 + ' ';
            }
            return street.trim();
        }
        return null;
    }
    
    private static Address getAddressFromType(List<Address> addresses, String type) {
        Address result = null;
        for(Address address : addresses)
        {
            if(address.AddressType == type)
            {
                if(address.IsDefault)
                {
                    result = address;
                    break;
                } 
                else 
                {
                    if(result == null)
                    {
                        result = address;
                    }
                }
            }
        }
        return result;
    }
    
    public class Address {
        private String AddressType {get; set;}
        private String AddressName {get; set;}
        private String StreetAddress {get; set;}
        private String StreetAddress2 {get; set;}
        private String Suburb {get; set;}
        private String City {get; set;}
        private String Region {get; set;}
        private String Country {get; set;}
        private String PostalCode {get; set;}
        private Boolean IsDefault {get; set;}
        private String DeliveryInstruction {get; set;}
    }   
    
    public class SellPriceTierReference {
        private String Reference {get; set;}
    }
    
    public class Customer {
        private List<Address> Addresses {get; set;}
        private String BankAccount {get; set;}
        private String BankBranch {get; set;}
        private String BankName {get; set;}
        private String ContactFirstName {get; set;}
        private String ContactLastName {get; set;}
        private String CreatedBy {get; set;}
        private String CreatedOn {get; set;}
        private UnleashedCurrency UnleashedCurrency {get; set;}
        private String CustomerCode {get; set;}
        private String CustomerName {get; set;}
        private String CustomerType {get; set;}
        private String DDINumber {get; set;}
        private String DeliveryInstruction {get; set;}
        private Double DiscountRate {get; set;}
        private String Email {get; set;}
        private String EmailCC {get; set;}
        private String FaxNumber {get; set;}
        private String GSTVATNumber {get; set;}
        private String Guid {get; set;}
        private String LastModifiedBy {get; set;}
        private String LastModifiedOn {get; set;}
        private String MobileNumber {get; set;}
        private String Notes {get; set;}
        private Boolean Obsolete {get; set;}
        private String PaymentTerm {get; set;}
        private String PhoneNumber {get; set;}
        private Boolean PrintInvoice {get; set;}
        private Boolean PrintPackingSlipInsteadOfInvoice {get; set;}
        private SalesPerson SalesPerson {get; set;}
        private String SellPriceTier {get; set;}
        private SellPriceTierReference SellPriceTierReference {get; set;}
        private Boolean StopCredit {get; set;}
        private Boolean Taxable {get; set;}
        private String TaxCode {get; set;}
        private Double TaxRate {get; set;}
        private String TollFreeNumber {get; set;}
        private String Website {get; set;}
        private String XeroCostOfGoodsAccount {get; set;}
        private String XeroSalesAccount {get; set;}
    }
}