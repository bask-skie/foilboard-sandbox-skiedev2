({
	init : function(component, event, helper) 
    {
        var recordId = component.get("v.eventId");
    	var action = component.get("c.getQuestions");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            component.set("v.showSpinner", false);
            var state = response.getState();
            if(state === "SUCCESS") {
                var data = response.getReturnValue();
                console.log(data);
                component.set("v.data", data);   
            }
            else if (state === "INCOMPLETE") {
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);
                var errorString = "Test: " + JSON.stringify(errors);
                console.log(errorString);                  
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type": "error",
                            "title": "Error",
                            "message": errors[0].message
                        });
                        toastEvent.fire();                          
                    }
                } else {
                    console.log("Unknown error");
                }
            }             
        });
    	$A.enqueueAction(action);         
	},
    reInit : function(component, event, helper) 
    {
        console.log('reInit');
        $A.get('e.force:refreshView').fire();
    },
	handleAnswerChange: function(component, event) 
    {
        var selectedOptionValue = event.getParam("value");
        var questions = component.get("v.data.questions");        
        
        var spltArray = selectedOptionValue.split(",");
        var answerId = spltArray[0];
        var questionId = spltArray[1];
        console.log(answerId, questionId);
     	
        for (var i=0; i<questions.length; i++)
        {
            if (questionId == questions[i].id)
            {
                for (var j=0; j<questions[i].answers.length; j++)
                {
                    if (answerId == questions[i].answers[j].id)
                    {                        
                        questions[i].answerId = answerId;
                        questions[i].requiredComment = questions[i].answers[j].requiredComment;
                        questions[i].requiredImage = questions[i].answers[j].requiredImage;
                        console.log(questions[i]);
                        break;
                    }
                }
                break;
            }
        }
        component.set("v.data.questions", questions); 
    },
    setPhoto: function (component, event, helper) 
    {        
        var questions = component.get("v.data.questions");
        var questionId = event.currentTarget.dataset.value;
        //var imageTargetId = "image_"+questionId;
        //var imageObj = document.getElementById(imageTargetId);

        var questionList = questions.filter(obj => {
          	return obj.id === questionId
        });
        var question = questionList[0];

        if(event.currentTarget.files.length === 0) 
        {
            return;
        }
        
        if(!event.currentTarget.files[0].type.match(/(image.*)/)) 
        {
            return alert('File is not supported');
        }
        else 
        {
            var file = event.currentTarget.files[0];
            helper.callImageSelected(component, event, file);
            document.getElementById("error_"+questionId).innerHTML = "";
        }     
    },
    handleFilesChange: function(component, event, helper)
    {
        var files = event.getSource().get("v.files");
        var questionId = event.getSource().get("v.name");
        console.log(questionId);
        console.log(files);
        var questions = component.get("v.data.questions");

        var questionList = questions.filter(obj => {
          	return obj.id === questionId
        });
        var question = questionList[0];
		
        for (var i=0; i<files.length; i++)
        {   
            if(!files[i].type.match(/(image.*)/)) 
            {
                return alert('File is not supported');
            }
            else 
            {
                var file = files[i];
                helper.imageSelected(component, event, file, questionId);
                document.getElementById("error_"+questionId).innerHTML = "";
            }            
        }             
    },
    removeImage: function(component, event, helper)
    {
        var name = event.getSource().get("v.name");
        var spltArray = name.split("_");
        var questionId = spltArray[0];
        var index = spltArray[1];
        
        var questions = component.get("v.data.questions");

        var questionList = questions.filter(obj => {
          	return obj.id === questionId
        });
        var question = questionList[0];
		question.images.splice(index, 1);        
        component.set("v.data.questions", questions);
    },
    submitForm: function(component, event, helper) 
    {
    	component.set("v.showSpinner", true);
        
        var recordId = component.get("v.eventId");
        var data = component.get("v.data");        
        var answerFields = component.find("answerFields");
        var allValid = false;

        if (answerFields)
        {
            if (answerFields.length)
            {
                allValid = answerFields.reduce(function (validFields, inputCmp) {                    
                    return validFields && inputCmp.reportValidity();
                }, true);               
            }
            else
            {
                allValid = answerFields.reportValidity();
            }
        }
        
        if(answerFields == undefined || answerFields == null)
        {
            //no question
            allValid = true;
        }
        
        for (var i=0; i<data.questions.length; i++)
        {
            if (data.questions[i].requiredImage == true)
            {
                if (data.questions[i].images.length == 0)
                {
                    document.getElementById("error_"+data.questions[i].id).innerHTML = "Please upload an image.";
                    allValid = false;
                }
            }
        }        
        if (allValid == false)
        {
            component.set("v.showSpinner", false);
            component.set("v.showMandatoryAnswerErrorModal", true);
        }
        else
        {
            var action = component.get("c.submit");
            action.setParams({"recordId": recordId, jsonString: JSON.stringify(data)});
            action.setCallback(this, function(response) {
                component.set("v.showSpinner", false);
                var state = response.getState();                
                if(state === "SUCCESS") 
                {                    
                    var navigate = component.get("v.navigateFlow");
                    navigate("NEXT");                           
                }
                else if (state === "INCOMPLETE") {
                    
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    console.log(errors);
                    var errorString = "Test: " + JSON.stringify(errors);
                    console.log(errorString);                  
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "type": "error",
                                "title": "Error",
                                "message": errors[0].message
                            });
                            toastEvent.fire();                          
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }                 
            });
            $A.enqueueAction(action);            
        }
    },
    closeMandatoryAnswerErrorModal : function(component, event, helper) 
    {
        component.set("v.showMandatoryAnswerErrorModal", false);
    }  
})