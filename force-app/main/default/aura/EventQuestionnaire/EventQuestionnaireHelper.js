({
    callImageSelected: function (component, event, file) 
    {
        var questions = component.get("v.data.questions");
        var questionId = event.currentTarget.dataset.value;
        var fileType = file.type;
        var fileName = file.name;

        var questionList = questions.filter(obj => {
          	return obj.id === questionId
        });
        var question = questionList[0];
        
        var img = {};
        img.imageType = fileType;
        img.imageFileName = fileName;
        
        console.log("file.type: ",fileType);
        
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var image = document.createElement('img');

        image.onload = function () {
            var max_size = 1024;
            var width = image.width;
            var height = image.height;

            if ((width > height) && (width > max_size)) {
                height *= max_size / width;
                width = max_size;
            } else if (height > max_size) {
                width *= max_size / height;
                height = max_size;
            }

            canvas.width = width;
            canvas.height = height;

            ctx.drawImage(image, 0, 0, width, height);
			
            var dataURL = canvas.toDataURL(fileType);
            
            console.log("dataURL: ",dataURL);
            
            var BASE64_MARKER = ';base64,';
            var parts, contentType, raw;
            
            parts = dataURL.split(BASE64_MARKER);
            contentType = parts[0].split(':')[1];
            
            img.imageBase64Data = parts[1];

            for (var i=0; i<questions.length; i++)
            {
                if (questions[i].id == question.id)
                {
                    questions[i].images.push(img);
                }
            } 
            component.set("v.data.questions", questions);
        };

        image.src = URL.createObjectURL(file);
    },
    imageSelected: function (component, event, file, questionId) 
    {
        var questions = component.get("v.data.questions");
        var fileType = file.type;
        var fileName = file.name;

        var questionList = questions.filter(obj => {
          	return obj.id === questionId
        });
        var question = questionList[0];
        
        var img = {};
        img.imageType = fileType;
        img.imageFileName = fileName;
        
        console.log("file.type: ",fileType);
        
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var image = document.createElement('img');

        image.onload = function () {
            var max_size = 1024;
            var width = image.width;
            var height = image.height;

            if ((width > height) && (width > max_size)) {
                height *= max_size / width;
                width = max_size;
            } else if (height > max_size) {
                width *= max_size / height;
                height = max_size;
            }

            canvas.width = width;
            canvas.height = height;

            ctx.drawImage(image, 0, 0, width, height);
			
            var dataURL = canvas.toDataURL(fileType);
            
            console.log("dataURL: ",dataURL);
            
            var BASE64_MARKER = ';base64,';
            var parts, contentType, raw;
            
            parts = dataURL.split(BASE64_MARKER);
            contentType = parts[0].split(':')[1];
            
            img.imageBase64Data = parts[1];

            for (var i=0; i<questions.length; i++)
            {
                if (questions[i].id == question.id)
                {
                    questions[i].images.push(img);
                }
            } 
            component.set("v.data.questions", questions);
        };

        image.src = URL.createObjectURL(file);
    }    
})